'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ui.router'
]).
    config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: 'search/search.html'
            })
            .state('library', {
                url: '/library',
                templateUrl: 'library/library.html'
            })
            .state('profile', {
                url: '/profile',
                templateUrl: 'profile/profile.html'
            });

        $urlRouterProvider.otherwise('/search');

    });